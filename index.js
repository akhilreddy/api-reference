const express = require("express"),
          app = express(),
         port = 3000;

let routes = require("./routes/index");

app.set("view engine", "ejs");
app.use(express.static(__dirname + '/public'));

app.get("/", (req, res)=>{
	res.redirect("/api/docs/")
})
app.use("/api/docs", routes); 

app.listen(port, ()=>{
    console.log("Server started on port " + port);
});