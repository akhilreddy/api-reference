const express = require("express"),
router = express.Router();

router.get("/", (req,res)=>{
	res.render("catalogList");
});

router.get("/getallcatalogs", (req, res)=>{
	res.render("getAllCatalogs");
})

module.exports = router;