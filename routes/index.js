const express = require("express"),
router= express.Router();

let catalogList = require("./catalogList");

router.get("/", (req,res)=>{
	res.render("index");
});

router.use("/cataloglist", catalogList);

module.exports = router;